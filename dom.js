let inputFrom = document.getElementById('inputEle')
let userInputElement = document.getElementById('user-input');
let checkBoxElement = document.getElementById('main-checkbox');
let todoLists = document.getElementById('todos-container')

let main = document.getElementById('main')

let itemsCount = document.getElementById('option')

let theme = document.getElementById('moon');
let endPart = document.getElementById('end-part')

let count = 0;

inputFrom.addEventListener('submit', addNewTodo);

function addNewTodo(e) {
    e.preventDefault()
    if (userInputElement.value == '') {
        alert('Please enter task!!')
    }
    else {
        let newTodo = document.createElement('li');
        newTodo.className = 'new-todo'
        newTodo.style.display = 'flex';
        newTodo.style.alignItems = 'center';

        let checkBox = document.createElement('input');
        checkBox.type = 'checkbox';
        checkBox.id = 'given';
        checkBox.classList.add('create-check-box')
        newTodo.appendChild(checkBox);

        let labelElement = document.createElement('label');
        labelElement.for = 'given'
        labelElement.id = 'todo-label'
        labelElement.textContent = userInputElement.value
        labelElement.classList.add('each-todo')
        newTodo.appendChild(labelElement);

        todoLists.appendChild(newTodo);
        count += 1;
        itemsCount.textContent = `${count} items left`

        let deletIcon = document.createElement('p')
        deletIcon.textContent = 'X'
        deletIcon.classList.add('delete-btn', 'delete-btn:hover');
        deletIcon.id = 'deleting'
        deletIcon.style.color = 'red';
        deletIcon.style.alignSelf = 'end';
        deletIcon.style.margin = '3px';
        newTodo.appendChild(deletIcon)

        userInputElement.value = ''

    }
}

todoLists.addEventListener('click', eventAligation)

function eventAligation(e) {
    if (e.target.checked) {
        e.target.parentElement.children[1].classList.add('strike')
        count -= 1;
        itemsCount.textContent = `${count} items left`
    }
    if (!e.target.checked) {
        e.target.parentElement.children[1].classList.remove('strike')
        count += 1;
        itemsCount.textContent = `${count} items left`
    }

    if (e.target.id == 'deleting') {
        (e.target.parentElement).remove()
        count = ((todoLists.children).length)
        if (count >= 0) {
            itemsCount.textContent = `${count} items left`
        }
    }

}

endPart.addEventListener('click', footerOptions)

function footerOptions(e) {
    if (e.target.id == 'all') {
        for (i = 0; i < (todoLists.children).length; i++) {
            (todoLists.children[i]).style.display = 'flex'
        }

    }

    if (e.target.id == 'active') {
        Array.from(document.querySelectorAll('.create-check-box')).map((each) => {
            if (each.checked) {
                each.parentElement.style.display = 'none'
            }
            else {
                each.parentElement.style.display = 'flex'
            }

        })
    }

    if (e.target.id == 'completed') {
        Array.from(document.querySelectorAll('.create-check-box')).map((each) => {
            if (each.checked) {
                each.parentElement.style.display = 'flex'
            }
            else {
                each.parentElement.style.display = 'none'
            }

        })
    }

    if (e.target.id == 'completed-tasks') {
        Array.from(document.querySelectorAll('.create-check-box')).map((each) => {
            if (each.checked) {
                each.parentElement.remove()
            }
        })
    }
}

theme.addEventListener('click', changeTheme)

function changeTheme(e) {
    e.target.classList.toggle('sun');
    (e.target.parentElement.parentElement).classList.toggle('dark-mode')
    main.classList.toggle('bg-dark')
    endPart.classList.toggle('end-part-dark')
    userInputElement.classList.toggle('user-input-dark')

    Array.from(document.querySelectorAll('.each-todo')).map((each)=>{
        each.classList.toggle('each-todo-dark')
    })
}
